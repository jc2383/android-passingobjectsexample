package com.example.passingobjectsexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendStudentPressed(View view) {
        // get studnet info
        EditText etName = (EditText) findViewById(R.id.etName);
        EditText etId = (EditText) findViewById(R.id.etId);
        EditText etGPA = (EditText) findViewById(R.id.etGPA);

        String name = etName.getText().toString();
        long studentId = Long.valueOf(etId.getText().toString());
        double gpa = Double.valueOf(etGPA.getText().toString());


        // @TODO:  Create a new student

        Student s = new Student(name, studentId, gpa);

        // @TODO:  Send that student to the next screen
        Intent i = new Intent(MainActivity.this, SecondActivity.class);
        i.putExtra("student", s);
        startActivity(i);

    }
}
