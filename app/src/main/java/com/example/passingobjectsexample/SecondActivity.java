package com.example.passingobjectsexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    final String TAG="OBJECTS-EXAMPLE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // receive the student
        Student s = (Student) getIntent().getSerializableExtra("student");
        Log.d(TAG, s.toString());

        // show in ui
        TextView t = (TextView) findViewById(R.id.tvResults);

        String msg = "Student name: " + s.getName() + "\n"
                    + "ID: " + s.getStudentId() + "\n"
                    + "GPA: " + s.getGpa() + "\n";

        t.setText(msg);
    }
}
